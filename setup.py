from distutils.core import setup

setup(name='mypackagename',
      version='0.0.1',
      description='python package test',
      author='doomleika',
      author_email='doomleika@gmail.com',
      url='https://gitlab.com/doomleika/gitlab-python-package',
      packages=['mypackage'],
     )
